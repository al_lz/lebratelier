<div id="fittin-room" class="container">
  <div class="row">
    {* AQUI VA EL MENU DE LA IZQUIERDA *}
    <div class="col-md-3 hidden-xs menu">
      <h3>CATEGORÍAS</h3>
      <ul>
        <li><a href="#1">¿como me queda?</a></li>
        <li><a href="#2">calculador de tallas</a></li>
        <li><a href="#3">problemas recurrentes</a></li>
        <!--<li><a href="#4">preguntas frecuentes</a></li>
        <li><a href="#5">equivalencia de tallas</a></li>
        <li><a href="#6">cuidado de tus prendas</a></li>-->
      </ul>
    </div>
    {* AQUI VA TODO EL CONTENIDO *}
    {* COMO ME QUEDA *}
    <div class="col-md-9">
        <h2 id="1" class="titulo">{l s='¿Como me queda?'}</h2>
        <p class="descripcion">{l s='La filosofía de Le Bratelier se basa en la confección artesanal de ropa interior que se ajusta completamente a tu cuerpo.
        El secreto para una sujeción perfecta es repartir el peso del pecho entre la espalda y la parte delantera. Recuerda que es la banda
        de la espalda -no los tirantes- la que presta máxima sujeción y reparte el peso equitativamente. El equilibrio sólo se consigue si el
        sujetador está colocado correctamente y sienta bien.'}
        </p>
        <div class="row">
          <!--<div class="franja-negra"></div>-->
          <div class="col-md-4 col-sm-4">
            <div class="imagen">
              <img src="{$img_dir}/fitting.jpg" alt="">
            </div>
            <h4>{l s='El cierre'}</h4>
            <p class="item-descripcion">{l s='Abrocha tu sujetador nuevo en el corchete exterior. Cuando el sujetador inevitablemente se dé un poco de sí, utiliza los corchetes del medio e interior.'}
            </p>
          </div>
          <div class="col-md-4 col-sm-4">
            <div class="imagen">
              <img src="{$img_dir}/fitting.jpg" alt="">
            </div>
            <h4>{l s='La espalda'}</h4>
            <p>{l s='Abrocha tu sujetador nuevo en el corchete exterior. Cuando el sujetador inevitablemente se dé un poco de sí, utiliza los corchetes del medio e interior.'}
            </p>
          </div>
          <div class="col-md-4 col-sm-4">
            <div class="imagen">
              <img src="{$img_dir}/fitting.jpg" alt="">
            </div>
            <h4>{l s='Los aros'}</h4>
            <p>{l s='Los aros deben quedar planos y apoyados en el tórax, nunca sobre el pecho. El recorrido del aro debe ser lo suﬁcientemente amplio como para respetar el contorno natural del pecho en su nacimiento, debajo del brazo. En este punto,los aros no deben clavarse ni hacer daño.'}
            </p>
          </div>
        </div>
        <div class="row">
        <!--<div class="franja-negra"></div>-->
        <div class="col-md-4 col-sm-4">
          <div class="imagen">
            <img src="{$img_dir}/fitting.jpg" alt="">
          </div>
          <h4>{l s='El cierre2'}</h4>
          <p class="item-descripcion">{l s='Las copas deben recoger suavementetodo el pecho. Un truco para colocarlocorrectamente es doblar el cuerpo haciadelante antes de abrochar el sujetador,acomodar bien el pecho dentro de lascopas, enderezarse y abrochar. Puedesayudarte de las manos para recolocar elpecho dentro de las copas, moviéndolode fuera hacia dentro. Si las copas seahuecan, son demasiado grandes, y si elpecho sobresale por encima o por loslados, son demasiado pequeñas.'}
          </p>
        </div>
        <div class="col-md-4 col-sm-4">
          <div class="imagen">
            <img src="{$img_dir}/fitting.jpg" alt="">
          </div>
          <h4>{l s='Los aros 2'}</h4>
          <p>{l s='Los tirantes se regulan a media posicióny se acomodan en la (casi impercepti-ble) hendidura que tenemos entre loshombros y el cuello. Una vez puesta laprenda, ajusta los tirantes a la medidaque quieras y por separado, ya quenormalmente cada pecho tiene untamaño diferente. Asegúrate de que noqueden demasiado tirantes ni desplacenla banda de la espalda hacia arriba.'}
          </p>
        </div>
        <div class="col-md-4 col-sm-4">
          <div class="imagen">
            <img src="{$img_dir}/fitting.jpg" alt="">
          </div>
          <h4>{l s='Los aros 3'}</h4>
          <p>{l s='La pieza central del sujetador (la parteentre las copas) es la que separa elpecho y debe quedar lo más plana posi-ble sobre el cuerpo, sin clavarse en lapiel.'}
          </p>
        </div>
        </div>
        <div class="row">
        <!--<div class="franja-negra"></div>-->


        {* CALCULADOR DE TALLAS *}

        <h2 id="2" class="titulo">Calculador de tallas</h2>
        <div class="row">
          <div class="col-md-12">
            <!--<iframe src="https://player.vimeo.com/video/103116470" width="100%" height="400px" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>-->

          </div>
        </div>
        
        <div class="row">
          <div class="col-md-6 col-sm-6">
	          <div class="imagen">
            <img src="{$img_dir}/fitting.jpg" alt="">
          </div>
            <h4>{l s='Paso 1. Talla de contorno.'}El cierre</h4>
            <p>{l s='Toma la medida de contorno justo por debajo del pecho, con lacinta métrica recta en todo su recorrido y ceñida (sin asﬁxiar).'}</p>
          </div>
          <div class="col-md-6 col-sm-6">
	          <div class="imagen">
            <img src="{$img_dir}/fitting.jpg" alt="">
          </div>
            <h4>{l s='Paso 2. Talla de pecho.'}El cierre</h4>
            <p>{l s='Ponte tu sujetador preferido (sin moldeado ni push-up) y toma lamedida de tu pecho a la altura del pezón, con la cinta métricarecta en todo su recorrido, que no quede ni muy apretada nimuy suelta.'}</p>
          </div>
          
        </div>
        
        <h2 id="4" class="titulo">Encuentra tu talla de sujetador</h2>
        
        <p style="margin-top:20px;">Elige tu talla de contorno: 
<select name="contorno" id="contorno">    
    <option value="0">---</option>
    <option value="64">64</option>
    <option value="65">65</option>
    <option value="66">66</option>
    <option value="67">67</option> 
    <option value="68">68</option>
    <option value="69">69</option>
    <option value="70">70</option>
    <option value="71">71</option>
    <option value="72">72</option>
    <option value="73">73</option> 
    <option value="74">74</option>
    <option value="75">75</option>
    <option value="76">76</option>
    <option value="77">77</option>
    <option value="78">78</option>
    <option value="79">79</option> 
    <option value="80">80</option>
    <option value="81">81</option>
    <option value="82">82</option>
    <option value="83">83</option> 
    <option value="84">84</option>
    <option value="85">85</option>
    <option value="86">86</option>
    <option value="87">87</option>
    <option value="88">88</option>
    <option value="89">89</option>
    <option value="90">90</option>
    <option value="91">91</option>
    <option value="92">92</option>
    <option value="93">93</option>    
</select></p>
<p>Elige tu talla de pecho:
<select name="pecho" id="pecho">    
    <option value="0">---</option>
    <option value="1">76</option>
    <option value="2">77</option>
    <option value="3">78</option>
     <option value="4">79</option>
      <option value="5">80</option>
       <option value="6">81</option>
        <option value="7">82</option>
 <option value="8">83</option>
 <option value="9">84</option>
 <option value="10">85</option>
 <option value="11">86</option>
 <option value="12">87</option>
 <option value="13">88</option>
 <option value="14">89</option>
  <option value="15">90</option>
  <option value="16">91</option>
  <option value="17">92</option>
  <option value="18">93</option>
</select></p>

<p>Talla y copa:
<textarea name="resultado-talla" id="resultado-talla"></textarea>
<textarea name="resultado-copa" id="resultado-copa"></textarea>
</p>
        
        
          <h2 id="3" class="titulo">Problemas recurrentes</h2>
          
          <div class="row">
          <div class="col-md-12 col-sm-12">
	          
            <p>{l s='La mayoría de las mujeres tenemos algunos problemas recurrentes que no sabemos cómo solucionar. En realidad son problemas derivados de llevar una talla incorrecta, que se resuelven fácilmente cambiando la talla de contorno, de copa o de ambas. La cinta métrica es una referencia muy valiosa, pero hay que ajustarla a la realidad. ¿Cuál es tu batalla con los sujetadores?'}</p>
          </div>
                    
        </div>



    </div>
  </div>

</div>
