{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie8"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<!--[if gt IE 8]> <html class="no-js ie9"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<html{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}>
	<head>
		<meta charset="utf-8" />
		<title>{$meta_title|escape:'html':'UTF-8'}</title>
		{if isset($meta_description) AND $meta_description}
			<meta name="description" content="{$meta_description|escape:'html':'UTF-8'}" />
		{/if}
		{if isset($meta_keywords) AND $meta_keywords}
			<meta name="keywords" content="{$meta_keywords|escape:'html':'UTF-8'}" />
		{/if}
		<meta name="generator" content="PrestaShop" />
		<meta name="robots" content="{if isset($nobots)}no{/if}index,{if isset($nofollow) && $nofollow}no{/if}follow" />
		<meta name="viewport" content="width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<link rel="icon" type="image/vnd.microsoft.icon" href="{$favicon_url}?{$img_update_time}" />
		<link rel="shortcut icon" type="image/x-icon" href="{$favicon_url}?{$img_update_time}" />
		{if isset($css_files)}
			{foreach from=$css_files key=css_uri item=media}
				<link rel="stylesheet" href="{$css_uri|escape:'html':'UTF-8'}" type="text/css" media="{$media|escape:'html':'UTF-8'}" />
			{/foreach}
		{/if}
		{if isset($js_defer) && !$js_defer && isset($js_files) && isset($js_def)}
			{$js_def}
			{foreach from=$js_files item=js_uri}
			<script type="text/javascript" src="{$js_uri|escape:'html':'UTF-8'}"></script>
			{/foreach}
		{/if}
		{$HOOK_HEADER}
		<link rel="stylesheet" href="http{if Tools::usingSecureMode()}s{/if}://fonts.googleapis.com/css?family=Open+Sans:300,600&amp;subset=latin,latin-ext" type="text/css" media="all" />
		<!--[if IE 8]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->

		<link rel="stylesheet" href="{$css_dir|escape:'html':'UTF-8'}carlos.css" type="text/css" media="{$media|escape:'html':'UTF-8'}" />
		<script src="{$js_dir}/carlos.js"></script>
		
		<script language="javascript">
$(document).ready(function(){
   $("#contorno").change(function () {
           $("#contorno option:selected").each(function () {
            elegido=$(this).val();
            $.post("http://www.lebratelier.com/themes/lebratelier/contorno.php", { elegido: elegido }, function(data){
            $("#pecho").html(data);
            }); 
                                   
        });
        
        $("#contorno option:selected").each(function () {
            elegido=$(this).val();
            $.post("http://www.lebratelier.com/themes/lebratelier/calcular-talla.php", { elegido: elegido }, function(data){
            $("#resultado-talla").html(data);
            }); 
                                   
        });
        
        $("#pecho option:selected").each(function () {
            elegido2=$(this).val();
            $.post("http://www.lebratelier.com/themes/lebratelier/calcular-copa.php", { elegido2: elegido2 }, function(data){
            $("#resultado-copa").html(data);
            }); 
                                   
        });
        
        
        
   });
   
    $("#pecho").change(function () {
	    
	    $("#contorno option:selected").each(function () {
            elegido=$(this).val();
            $.post("http://www.lebratelier.com/themes/lebratelier/calcular-talla.php", { elegido: elegido }, function(data){
            $("#resultado-talla").html(data);
            }); 
                                   
        });
   
   $("#pecho option:selected").each(function () {
            elegido2=$(this).val();
            $.post("http://www.lebratelier.com/themes/lebratelier/calcular-copa.php", { elegido2: elegido2 }, function(data){
            $("#resultado-copa").html(data);
            }); 
                                   
        });
        
         });
   
});
</script>

<!--<script language="JavaScript" type="text/JavaScript">
    $(document).ready(function(){
        $("#contorno").change(function(event){
            var id = $("#contorno").find(':selected').val();
            $("#select2").load('genera-select.php?id='+id);
        });
    });
</script>-->

	</head>
	<body{if isset($page_name)} id="{$page_name|escape:'html':'UTF-8'}"{/if} class="{if isset($page_name)}{$page_name|escape:'html':'UTF-8'}{/if}{if isset($body_classes) && $body_classes|@count} {implode value=$body_classes separator=' '}{/if}{if $hide_left_column} hide-left-column{else} show-left-column{/if}{if $hide_right_column} hide-right-column{else} hide-right-column{/if}{if isset($content_only) && $content_only} content_only{/if} lang_{$lang_iso}">
	{if !isset($content_only) || !$content_only}
		{if isset($restricted_country_mode) && $restricted_country_mode}
			<div id="restricted-country">
				<p>{l s='You cannot place a new order from your country.'}{if isset($geolocation_country) && $geolocation_country} <span class="bold">{$geolocation_country|escape:'html':'UTF-8'}</span>{/if}</p>
			</div>
		{/if}
		<div id="fijo">
			<ul>
				<li><a href="#newsletter_block_left">Suscríbete al Newsletter</a></li>
				<li><a href="{$base_dir}contactanos">Contacta</a></li>
				<!--<li><a href="">Chat Online</a></li>-->
				<li class="pull-right"><a href="{$base_dir}fitting-room">Fittin Room</a></li>
				<li class="pull-right"><a id="link_filtros" href="#">Filtros</a></li>

				<!-- {if $page_name =='sitemap'} -->
				<!-- AQUI TENDRÉ QUE DECIDIR CUANDO ESTA EN LA TIENDA -->
				<!-- {/if} -->
				<!-- <li><a href=""></a></li> -->
			</ul>
		</div>
		<div id="page" class="buu">
			<div class="header-container">
				<header id="header">
					<div class="banner">
						<div class="container">
							<div class="row hidden-xs">
								{*hook h="htmlbox"*}
								<!--<p>{l s='Texto cabecera'}</p>-->
																								
								<p>{l s='¡Realiza'} <span>{l s='nuestro test'}</span> {l s='para saber tu medida! Productos realizados en España'}</p>
								{* {hook h="displayBanner"} *}
							</div>
						</div>
					</div>
					<div class="nav">
						<div class="container-fluid">
							<div class="row hidden-xs">
								<a id="nav-toggle" class="transform-container menu-lateral-izquierdo" href="#"><img src="{$img_dir}menu.png" alt="" /><!--<span></span>--></a>
								<a href="{$base_dir}" class="logo-medio"><img id="logo_nav" class="transform-container" src="{$img_dir}logo.png" alt="lebratelier logo"></a>

								<nav>{hook h="displayNav"}</nav>
							</div>
							<div class="row hidden-md hidden-lg hidden-sm" style="height:45px;">
									<nav id="navbar" class="navBar fixed ready"><button id="button_nav" role="button" ng-click="showMenu()" class="menu transform-container">Menu</button></nav>
									<img id="logo_nav" class="transform-container" src="{$img_dir}logo.png" alt="lebratelier logo">

									<div class="shopping_cart shopping_cart_xs">
										<a href="{$link->getPageLink($order_process, true)|escape:'html':'UTF-8'}" title="{l s='View my shopping cart' mod='blockcart'}" rel="nofollow">
											{* <b>{l s='Cart' mod='blockcart'}</b> *}
											<span class="ajax_cart_quantity{if $cart_qties == 0} unvisible{/if}" style="padding-right:5px;">{$cart_qties}</span>
											<!-- {* <span class="ajax_cart_product_txt{if $cart_qties != 1} unvisible{/if}">{l s='Product' mod='blockcart'}</span> *} -->
											<!-- <span class="ajax_cart_product_txt_s{if $cart_qties < 2} unvisible{/if}">{l s='Products' mod='blockcart'}</span> -->
											<span class="ajax_cart_total{if $cart_qties == 0} unvisible{/if}">
												{if $cart_qties > 0}
													{if $priceDisplay == 1}
														{assign var='blockcart_cart_flag' value='Cart::BOTH_WITHOUT_SHIPPING'|constant}
														{convertPrice price=$cart->getOrderTotal(false, $blockcart_cart_flag)}
													{else}
														{assign var='blockcart_cart_flag' value='Cart::BOTH_WITHOUT_SHIPPING'|constant}
														{convertPrice price=$cart->getOrderTotal(true, $blockcart_cart_flag)}
													{/if}
												{/if}
											</span>
											<span class="ajax_cart_no_product{if $cart_qties > 0} unvisible{/if}" style="padding-right:5px;">0</span>
											{if $ajax_allowed && isset($blockcart_top) && !$blockcart_top}
												<span class="block_cart_expand{if !isset($colapseExpandStatus) || (isset($colapseExpandStatus) && $colapseExpandStatus eq 'expanded')} unvisible{/if}">&nbsp;</span>
												<span class="block_cart_collapse{if isset($colapseExpandStatus) && $colapseExpandStatus eq 'collapsed'} unvisible{/if}">&nbsp;</span>
											{/if}
										</a>
										{if !$PS_CATALOG_MODE}
											<div class="cart_block block exclusive" style="display:block;">
												<div class="block_content">
													<!-- block list of products -->
													<div class="cart_block_list">
													<!-- <div class="cart_block_list{if isset($blockcart_top) && !$blockcart_top}{if isset($colapseExpandStatus) && $colapseExpandStatus eq 'expanded' || !$ajax_allowed || !isset($colapseExpandStatus)} expanded{else} collapsed unvisible{/if}{/if}"> -->
														{if $products}
															<dl class="products">
																{foreach from=$products item='product' name='myLoop'}
																	{assign var='productId' value=$product.id_product}
																	{assign var='productAttributeId' value=$product.id_product_attribute}
																	<dt data-id="cart_block_product_{$product.id_product|intval}_{if $product.id_product_attribute}{$product.id_product_attribute|intval}{else}0{/if}_{if $product.id_address_delivery}{$product.id_address_delivery|intval}{else}0{/if}" class="{if $smarty.foreach.myLoop.first}first_item{elseif $smarty.foreach.myLoop.last}last_item{else}item{/if}">
																		<a class="cart-images" href="{$link->getProductLink($product.id_product, $product.link_rewrite, $product.category)|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}"><img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'cart_default')}" alt="{$product.name|escape:'html':'UTF-8'}" /></a>
																		<div class="cart-info">
																			<div class="product-name">
																				<span class="quantity-formated"><span class="quantity">{$product.cart_quantity}</span>&nbsp;x&nbsp;</span><a class="cart_block_product_name" href="{$link->getProductLink($product, $product.link_rewrite, $product.category, null, null, $product.id_shop, $product.id_product_attribute)|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}">{$product.name|truncate:13:'...'|escape:'html':'UTF-8'}</a>
																			</div>
																			{if isset($product.attributes_small)}
																				<div class="product-atributes">
																					<a href="{$link->getProductLink($product, $product.link_rewrite, $product.category, null, null, $product.id_shop, $product.id_product_attribute)|escape:'html':'UTF-8'}" title="{l s='Product detail' mod='blockcart'}">{$product.attributes_small}</a>
																				</div>
																			{/if}
																			<span class="price">
																				{if !isset($product.is_gift) || !$product.is_gift}
																					{if $priceDisplay == $smarty.const.PS_TAX_EXC}{displayWtPrice p="`$product.total`"}{else}{displayWtPrice p="`$product.total_wt`"}{/if}
								                                                    <div id="hookDisplayProductPriceBlock-price">
								                                                        {hook h="displayProductPriceBlock" product=$product type="price" from="blockcart"}
								                                                    </div>
																				{else}
																					{l s='Free!' mod='blockcart'}
																				{/if}
																			</span>
																		</div>
																		<span class="remove_link">
																			{if !isset($customizedDatas.$productId.$productAttributeId) && (!isset($product.is_gift) || !$product.is_gift)}
																				<a class="ajax_cart_block_remove_link" href="{$link->getPageLink('cart', true, NULL, "delete=1&id_product={$product.id_product|intval}&ipa={$product.id_product_attribute|intval}&id_address_delivery={$product.id_address_delivery|intval}&token={$static_token}")|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='remove this product from my cart' mod='blockcart'}">&nbsp;</a>
																			{/if}
																		</span>
																	</dt>
																	{if isset($product.attributes_small)}
																		<dd data-id="cart_block_combination_of_{$product.id_product|intval}{if $product.id_product_attribute}_{$product.id_product_attribute|intval}{/if}_{$product.id_address_delivery|intval}" class="{if $smarty.foreach.myLoop.first}first_item{elseif $smarty.foreach.myLoop.last}last_item{else}item{/if}">
																	{/if}
																	<!-- Customizable datas -->
																	{if isset($customizedDatas.$productId.$productAttributeId[$product.id_address_delivery])}
																		{if !isset($product.attributes_small)}
																			<dd data-id="cart_block_combination_of_{$product.id_product|intval}_{if $product.id_product_attribute}{$product.id_product_attribute|intval}{else}0{/if}_{if $product.id_address_delivery}{$product.id_address_delivery|intval}{else}0{/if}" class="{if $smarty.foreach.myLoop.first}first_item{elseif $smarty.foreach.myLoop.last}last_item{else}item{/if}">
																		{/if}
																		<ul class="cart_block_customizations" data-id="customization_{$productId}_{$productAttributeId}">
																			{foreach from=$customizedDatas.$productId.$productAttributeId[$product.id_address_delivery] key='id_customization' item='customization' name='customizations'}
																				<li name="customization">
																					<div data-id="deleteCustomizableProduct_{$id_customization|intval}_{$product.id_product|intval}_{$product.id_product_attribute|intval}_{$product.id_address_delivery|intval}" class="deleteCustomizableProduct">
																						<a class="ajax_cart_block_remove_link" href="{$link->getPageLink('cart', true, NULL, "delete=1&id_product={$product.id_product|intval}&ipa={$product.id_product_attribute|intval}&id_customization={$id_customization|intval}&token={$static_token}")|escape:'html':'UTF-8'}" rel="nofollow">&nbsp;</a>
																					</div>
																					{if isset($customization.datas.$CUSTOMIZE_TEXTFIELD.0)}
																						{$customization.datas.$CUSTOMIZE_TEXTFIELD.0.value|replace:"<br />":" "|truncate:28:'...'|escape:'html':'UTF-8'}
																					{else}
																						{l s='Customization #%d:' sprintf=$id_customization|intval mod='blockcart'}
																					{/if}
																				</li>
																			{/foreach}
																		</ul>
																		{if !isset($product.attributes_small)}</dd>{/if}
																	{/if}
																	{if isset($product.attributes_small)}</dd>{/if}
																{/foreach}
															</dl>
														{/if}
														<p class="cart_block_no_products{if $products} unvisible{/if}">
															{l s='No products' mod='blockcart'}
														</p>
														{if $discounts|@count > 0}
															<table class="vouchers{if $discounts|@count == 0} unvisible{/if}">
																{foreach from=$discounts item=discount}
																	{if $discount.value_real > 0}
																		<tr class="bloc_cart_voucher" data-id="bloc_cart_voucher_{$discount.id_discount|intval}">
																			<td class="quantity">1x</td>
																			<td class="name" title="{$discount.description}">
																				{$discount.name|truncate:18:'...'|escape:'html':'UTF-8'}
																			</td>
																			<td class="price">
																				-{if $priceDisplay == 1}{convertPrice price=$discount.value_tax_exc}{else}{convertPrice price=$discount.value_real}{/if}
																			</td>
																			<td class="delete">
																				{if strlen($discount.code)}
																					<a class="delete_voucher" href="{$link->getPageLink("$order_process", true)}?deleteDiscount={$discount.id_discount|intval}" title="{l s='Delete' mod='blockcart'}" rel="nofollow">
																						<i class="icon-remove-sign"></i>
																					</a>
																				{/if}
																			</td>
																		</tr>
																	{/if}
																{/foreach}
															</table>
														{/if}
														<div class="cart-prices">
															<div class="cart-prices-line first-line">
																<span class="price cart_block_shipping_cost ajax_cart_shipping_cost{if !($page_name == 'order-opc') && $shipping_cost_float == 0 && (!isset($cart->id_address_delivery) || !$cart->id_address_delivery)} unvisible{/if}">
																	{if $shipping_cost_float == 0}
																		 {if !($page_name == 'order-opc') && (!isset($cart->id_address_delivery) || !$cart->id_address_delivery)}{l s='To be determined' mod='blockcart'}{else}{l s='Free shipping!' mod='blockcart'}{/if}
																	{else}
																		{$shipping_cost}
																	{/if}
																</span>
																<span{if !($page_name == 'order-opc') && $shipping_cost_float == 0 && (!isset($cart->id_address_delivery) || !$cart->id_address_delivery)} class="unvisible"{/if}>
																	{l s='Shipping' mod='blockcart'}
																</span>
															</div>
															{if $show_wrapping}
																<div class="cart-prices-line">
																	{assign var='cart_flag' value='Cart::ONLY_WRAPPING'|constant}
																	<span class="price cart_block_wrapping_cost">
																		{if $priceDisplay == 1}
																			{convertPrice price=$cart->getOrderTotal(false, $cart_flag)}{else}{convertPrice price=$cart->getOrderTotal(true, $cart_flag)}
																		{/if}
																	</span>
																	<span>
																		{l s='Wrapping' mod='blockcart'}
																	</span>
															   </div>
															{/if}
															{if $show_tax && isset($tax_cost)}
																<div class="cart-prices-line">
																	<span class="price cart_block_tax_cost ajax_cart_tax_cost">{$tax_cost}</span>
																	<span>{l s='Tax' mod='blockcart'}</span>
																</div>
															{/if}
															<div class="cart-prices-line last-line">
																<span class="price cart_block_total ajax_block_cart_total">{$total}</span>
																<span>{l s='Total' mod='blockcart'}</span>
															</div>
															{if $use_taxes && $display_tax_label == 1 && $show_tax}
																<p>
																{if $priceDisplay == 0}
																	{l s='Prices are tax included' mod='blockcart'}
																{elseif $priceDisplay == 1}
																	{l s='Prices are tax excluded' mod='blockcart'}
																{/if}
																</p>
															{/if}
														</div>
														<p class="cart-buttons">
															<a id="button_order_cart" class="btn btn-default button button-small" href="{$link->getPageLink("$order_process", true)|escape:"html":"UTF-8"}" title="{l s='Check out' mod='blockcart'}" rel="nofollow">
																<span>
																	{l s='Check out' mod='blockcart'}<i class="icon-chevron-right right"></i>
																</span>
															</a>
														</p>
													</div>
												</div>
											</div><!-- .cart_block -->

										{/if}
									</div>
							</div>
						</div>
					</div>
					<div>
						<div class="container">
							<div class="row">
								<figure id="overlay" class="overlay ng-scope" ng-controller="OverlayController" ng-init="init()" ng-click="closeOverlay()">
					      	<button>X</button>
					      </figure>
								<aside id="sidemenu" class="sidemenu ng-scope">
									<!-- <div id="header_logo">
										<a href="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}" title="{$shop_name|escape:'html':'UTF-8'}">
											<img class="logo img-responsive" src="{$logo_url}" alt="{$shop_name|escape:'html':'UTF-8'}"{if isset($logo_image_width) && $logo_image_width} width="{$logo_image_width}"{/if}{if isset($logo_image_height) && $logo_image_height} height="{$logo_image_height}"{/if}/>
										</a>
									</div> -->
									{if isset($HOOK_TOP)}{$HOOK_TOP}{/if}
								</aside>

							</div>
						</div>
					</div>
				</header>
			</div>
			<div class="columns-container transform-container">
			<section id="filters" ng-controller="FilterOverlayController" class="ng-scope">

				<button class="closeFilters" ng-click="closeFilters()">x<span class="label">CLOSE</span></button>

				<div id="filterLists">
						<!--<div class="filterList">
								<h3>Gender</h3>

								<div class="scrollpane"><ul data-type="gender"><li>Men</li><li>Women</li></ul></div>
						</div>-->
						<div class="filterList">
								<h3>Producto</h3>

								<div class="scrollpane"><ul data-type="product"><li>Bra</li><li>Bikini</li><li>Culotte</li><li>Tanga</li></ul></div>
						</div>
						<div class="filterList">
								<h3>Color</h3>

								<div class="scrollpane"><ul data-type="color"><li>Rosa</li><li>Azul</li><!--<li>Green</li><li>Grey</li><li>Red</li><li>White</li>--></ul></div>
						</div>
						<!--<div class="filterList">
								<h3>Style</h3>

								<div class="scrollpane"><ul data-type="style"><li>High 1</li><li>High 2</li><li>Low 1</li><li>Low 2</li><li>Low 3</li><li>Mid 1</li><li>Mid 2</li></ul></div>
						</div>-->
						<div class="filterList">
								<h3>Talla</h3>

								<div class="scrollpane"><ul data-type="size"><!--<li class="key">ES</li>--><li data-size="EU 36 | US 03 | UK 02">80</li><li data-size="EU 37 | US 04 | UK 03">85</li><li data-size="EU 38 | US 05 | UK 04">90</li><li data-size="EU 39 | US 06 | UK 05">95</li><li data-size="EU 40 | US 07 | UK 06">100</li><li data-size="EU 41 | US 08 | UK 07">105</li><!--<li data-size="EU 42 | US 09 | UK 08">42 | 09 | 08</li><li data-size="EU 43 | US 10 | UK 09">43 | 10 | 09</li><li data-size="EU 44 | US 11 | UK 10">44 | 11 | 10</li><li data-size="EU 45 | US 12 | UK 11">45 | 12 | 11</li><li data-size="EU 46 | US 13 | UK 12">46 | 13 | 12</li><li data-size="EU 47 | US 14 | UK 13">47 | 14 | 13</li>--></ul></div>
						</div>
				</div>

				<footer><p>You can select several options simultaneously</p></footer>
				<nav class="footerbuttons show">
						<button class="inverse" role="button" ng-click="clearFilters()"><span>Borrar</span><span class="hover">Borrar</span></button>
						<button role="button" ng-click="submitFilters()" class="disabled"><span>Aplicar</span><span class="hover">Aplicar</span></button>
				</nav>
			</section>
				<div id="slider_row" class="row" style="margin-right:0px;padding:0px;">
					<div id="top_column" style="margin:0px;padding:0px;" class="center_column col-xs-12 col-sm-12">{hook h="displayTopColumn"}</div>
				</div>
				<div id="columns" class="container-fluid">
					{if $page_name !='index' && $page_name !='pagenotfound'}
						{include file="$tpl_dir./breadcrumb.tpl"}
					{/if}

					<div class="row">
						{if isset($left_column_size) && !empty($left_column_size)}
						<div id="left_column" class="column col-xs-12 col-sm-{$left_column_size|intval}">{$HOOK_LEFT_COLUMN}</div>
						{/if}
						{if isset($left_column_size) && isset($right_column_size)}{assign var='cols' value=(12 - $left_column_size - $right_column_size)}{else}{assign var='cols' value=12}{/if}
						<div id="center_column" class="center_column col-xs-12 col-sm-{$cols|intval}">
	{/if}
