{if $MENU != ''}
	<!-- Menu -->
	<div id="block_top_menu" class="sf-contener clearfix col-lg-12">
		<!-- <div class="cat-title"><span>{l s="Menu" mod="blocktopmenu"}</span></div> -->
		<div style="width:100%;padding:0 40px;text-transform:uppercase;">
			<a href="/">Inicio</a>
			<a href="/mi-cuenta" style="float:right;">Identificate</a>
		</div>
		<ul class="sf-menu clearfix menu-content">
			{if $MENU_SEARCH}
				<li class="sf-search noBack" style="">
					<form id="searchbox" action="{$link->getPageLink('search')|escape:'html':'UTF-8'}" method="get">
						<p>
							<input type="hidden" name="controller" value="search" />
							<input type="hidden" value="position" name="orderby"/>
							<input type="hidden" value="desc" name="orderway"/>
							<span></span>
							<input type="text" name="search_query" value="{if isset($smarty.get.search_query)}{$smarty.get.search_query|escape:'html':'UTF-8'}{/if}" />
							<button type="submit" name="submit_search" class="btn btn-default button-search">
								<span>Search</span>
							</button>
						</p>
					</form>
				</li>
			{/if}
			<li data-toggle="collapse" data-target="#tienda"><a href="#">TIENDA</a>
				<ul id="tienda" class="collapse">
				{$MENU}
				</ul>
			</li>
			<li><a href="#">LOOKBOOK</a></li>
			<li ><a href="#">NOSOTROS</a></li>
			<li ><a href="#">BLOG</a></li>

		</ul>
	</div>
	<!--/ Menu -->
{/if}
