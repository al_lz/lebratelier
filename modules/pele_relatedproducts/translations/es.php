<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{pele_relatedproducts}lebratelier>pele_relatedproducts_16_347da0da598694c57fff1cbaa197d615'] = 'Te puede interesar';
$_MODULE['<{pele_relatedproducts}lebratelier>pele_relatedproducts_16_36fa3466fb5e925fef12e9cc9d2c1c5e'] = 'Te puede interesar';
$_MODULE['<{pele_relatedproducts}lebratelier>pele_relatedproducts_f4459b9d2f7846d18587200a8e5c56ef'] = 'Pele - Productos relacionados';
$_MODULE['<{pele_relatedproducts}lebratelier>pele_relatedproducts_7f7167d6ba0534240be081c8632a7a63'] = 'Muestra productos relacionados por etiqueta';
$_MODULE['<{pele_relatedproducts}lebratelier>pele_relatedproducts_tab_347da0da598694c57fff1cbaa197d615'] = 'Productos Relacionados';
$_MODULE['<{pele_relatedproducts}lebratelier>pele_relatedproducts_5c673ea58d70a752dae27eedf4198ce7'] = 'No hay productos relacionados';
