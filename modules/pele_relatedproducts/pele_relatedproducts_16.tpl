{if count($peleRelatedProducts) > 0}
<section class="page-product-box blockpeleproductsrelated">

	<h4 class="peleproductsrelated_h3 page-product-heading h4 optima"><span>{l s='Related Products' mod='pele_relatedproducts'}</span></h4>

	<div id="texto-conjuntos">
<p>{l s='Texto a juego con' mod='pele_relatedproducts'}</p>
</div>

	<div id="peleproductsrelated_list" class="clearfix" >
		<ul id="bxslider_pele" class="bxslider clearfix">
		 {foreach from=$peleRelatedProducts item='peleRelatedProduct' name=categoryProduct}
			<li class="related col-xs-12 col-sm-3 col-md-3 first-in-line last-line first-item-of-tablet-line first-item-of-mobile-line">
				<a href="{$peleRelatedProduct.link}" class="lnk_img product-imagee" title="{$peleRelatedProduct.name|htmlspecialchars}">
          <img style="width:100%;" src="{$link->getImageLink($peleRelatedProduct.image.link_rewrite, $peleRelatedProduct.image.id_image, 'home_default')|escape:'html':'UTF-8'}" alt="{$peleRelatedProduct.name|htmlspecialchars}" /></a>

				<h5 class="product-name optima mayusculas">
					<a href="{$peleRelatedProduct.link}" title="{$peleRelatedProduct.name|htmlspecialchars}">{$peleRelatedProduct.name|escape:'html':'UTF-8'}</a>
				</h5>
				<p class="related">{convertPrice price=$peleRelatedProduct.displayed_price}</p>
				<br />
			</li>
		{/foreach}
		</ul>
	</div>
</section>
{/if}


<!--<script type="text/javascript">
$(document).ready(function() {
	if (!!$.prototype.bxSlider)
		$('#bxslider_pele').bxSlider({
			minSlides: 1,
			maxSlides: 4,
			slideWidth: 260,
			slideMargin: 20,
			pager: false,
			nextText: '',
			prevText: '',
			moveSlides:1,
			infiniteLoop:false,
			hideControlOnEnd: true
		});
});
</script>-->
