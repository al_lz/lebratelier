<div class="container">
  <h2 class="titulo">¿Como me queda?</h2>
  <p>La filosofía de Le Bratelier se basa en la confección artesanal de ropa interior que se ajusta completamente a tu cuerpo.
  El secreto para una sujeción perfecta es repartir el peso del pecho entre la espalda y la parte delantera. Recuerda que es la banda
  de la espalda -no los tirantes- la que presta máxima sujeción y reparte el peso equitativamente. El equilibrio sólo se consigue si el
  sujetador está colocado correctamente y sienta bien. 
  </p>
  <div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-3"></div>
    <div class="col-md-3"></div>
  </div>
  <div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-3"></div>
    <div class="col-md-3"></div>
  </div>
  <div class="row">
    <div class="col-md-offset-1"></div>
  </div>
</div>
