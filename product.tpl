{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{include file="$tpl_dir./errors.tpl"}
{if $errors|@count == 0}
	{if !isset($priceDisplayPrecision)}
		{assign var='priceDisplayPrecision' value=2}
	{/if}
	{if !$priceDisplay || $priceDisplay == 2}
		{assign var='productPrice' value=$product->getPrice(true, $smarty.const.NULL, 6)}
		{assign var='productPriceWithoutReduction' value=$product->getPriceWithoutReduct(false, $smarty.const.NULL)}
	{elseif $priceDisplay == 1}
		{assign var='productPrice' value=$product->getPrice(false, $smarty.const.NULL, 6)}
		{assign var='productPriceWithoutReduction' value=$product->getPriceWithoutReduct(true, $smarty.const.NULL)}
	{/if}
<div itemscope itemtype="http://schema.org/Product">
	<meta itemprop="url" content="{$link->getProductLink($product)}">
	<div class="primary_block row primary_block_product" style="{if $content_only} {l s='margin-top:0px;'}{/if}">
		{if !$content_only}
			<div class="container">
				<div class="top-hr"></div>
			</div>
		{/if}
		{if isset($adminActionDisplay) && $adminActionDisplay}
			<div id="admin-action" class="container">
				<p class="alert alert-info">{l s='This product is not visible to your customers.'}
					<input type="hidden" id="admin-action-product-id" value="{$product->id}" />
					<a id="publish_button" class="btn btn-default button button-small" href="#">
						<span>{l s='Publish'}</span>
					</a>
					<a id="lnk_view" class="btn btn-default button button-small" href="#">
						<span>{l s='Back'}</span>
					</a>
				</p>
				<p id="admin-action-result"></p>
			</div>
		{/if}
		{if isset($confirmation) && $confirmation}
			<p class="confirmation">
				{$confirmation}
			</p>
		{/if}
		<!-- left infos-->
		<div class="pb-left-column col-xs-12 col-sm-7 col-md-7">
			<div id="franja-single"></div>
			<!-- product img-->
			<div id="image-block" class="clearfix">
				{if $product->new}
					<!-- <span class="new-box" >
						<span class="new-label">{l s='New'}</span>
					</span> -->
				{/if}
				<!--{if $product->on_sale}
					<span class="sale-box no-print">
						<span class="sale-label">{l s='Sale!'}</span>
					</span>
				{elseif $product->specificPrice && $product->specificPrice.reduction && $productPriceWithoutReduction > $productPrice}
					<span class="discount">{l s='Reduced price!'}</span>
				{/if}-->
				{if $have_image}
					<span id="view_full_size" style="display:block;">
						{if $jqZoomEnabled && $have_image && !$content_only}
							<a class="jqzoom" title="{if !empty($cover.legend)}{$cover.legend|escape:'html':'UTF-8'}{else}{$product->name|escape:'html':'UTF-8'}{/if}" rel="gal1" href="{$link->getImageLink($product->link_rewrite, $cover.id_image, 'thickbox_default')|escape:'html':'UTF-8'}">
								<img itemprop="image" src="{$link->getImageLink($product->link_rewrite, $cover.id_image, 'large_default')|escape:'html':'UTF-8'}" title="{if !empty($cover.legend)}{$cover.legend|escape:'html':'UTF-8'}{else}{$product->name|escape:'html':'UTF-8'}{/if}" alt="{if !empty($cover.legend)}{$cover.legend|escape:'html':'UTF-8'}{else}{$product->name|escape:'html':'UTF-8'}{/if}"/>
							</a>
						{else}
							<img id="bigpic" itemprop="image " src="{$link->getImageLink($product->link_rewrite, $cover.id_image, 'large_default')|escape:'html':'UTF-8'}" title="{if !empty($cover.legend)}{$cover.legend|escape:'html':'UTF-8'}{else}{$product->name|escape:'html':'UTF-8'}{/if}" alt="{if !empty($cover.legend)}{$cover.legend|escape:'html':'UTF-8'}{else}{$product->name|escape:'html':'UTF-8'}{/if}" width="{$largeSize.width}" height="{$largeSize.height}"/>
							{if !$content_only}
								<div class="">

									<span id="foto_izq" class="col-md-2 col-sm-2 col-xs-2 span_link">&nbsp;</span>
									{* <span class="col-md-6 span_link no-print">{l s='View larger'}</span> *}
									<span id="foto_grande" class="col-md-8  col-sm-8 col-xs-8 span_link no-print"><span><a id="foto_qv" data-height="{$largeSize.height}" data-width="{$largeSize.width}" href="{$link->getImageLink($product->link_rewrite, $cover.id_image, 'large_default')|escape:'html':'UTF-8'}" rel="{$link->getImageLink($product->link_rewrite, $cover.id_image, 'large_default')|escape:'html':'UTF-8'}" style="display:block;width:100%;height:100%;"></a></span></span>
									<span id="foto_derecha" class="col-md-2  col-sm-2 col-xs-2 span_link"><span></span></span>

								</div>
							{/if}
						{/if}
					</span>
				{else}
					<span id="view_full_size">
						<img itemprop="image" src="{$img_prod_dir}{$lang_iso}-default-large_default.jpg" id="bigpic" alt="" title="{$product->name|escape:'html':'UTF-8'}" width="{$largeSize.width}" height="{$largeSize.height}"/>
						{if !$content_only}
							<span class="span_link">
								{l s='View larger'}
							</span>

						{/if}
					</span>
				{/if}
			</div> <!-- end image-block -->
			{if isset($images) && count($images) > 0}
				<!-- thumbnails -->
				<div id="views_block" class="clearfix {if isset($images) && count($images) < 2}hidden{/if}" style="display:none;">
					{if isset($images) && count($images) > 2}
						<span class="view_scroll_spacer">
							<a id="view_scroll_left" class="" title="{l s='Other views'}" href="javascript:{ldelim}{rdelim}">
								{l s='Previous'}
							</a>
						</span>
					{/if}
					<div id="thumbs_list" style="">
						<ul id="thumbs_list_frame">
						{if isset($images)}
							{foreach from=$images item=image name=thumbnails}
								{assign var=imageIds value="`$product->id`-`$image.id_image`"}
								{if !empty($image.legend)}
									{assign var=imageTitle value=$image.legend|escape:'html':'UTF-8'}
								{else}
									{assign var=imageTitle value=$product->name|escape:'html':'UTF-8'}
								{/if}
								<li id="thumbnail_{$image.id_image}"{if $smarty.foreach.thumbnails.last} class="last"{/if}>
									<a{if $jqZoomEnabled && $have_image && !$content_only} href="javascript:void(0);" rel="{literal}{{/literal}gallery: 'gal1', smallimage: '{$link->getImageLink($product->link_rewrite, $imageIds, 'large_default')|escape:'html':'UTF-8'}',largeimage: '{$link->getImageLink($product->link_rewrite, $imageIds, 'thickbox_default')|escape:'html':'UTF-8'}'{literal}}{/literal}"{else} href="{$link->getImageLink($product->link_rewrite, $imageIds, 'thickbox_default')|escape:'html':'UTF-8'}"	data-fancybox-group="other-views" class="fancybox{if $image.id_image == $cover.id_image} shown{/if}"{/if} title="{$imageTitle}">
										<img class="img-responsive" id="thumb_{$image.id_image}" src="{$link->getImageLink($product->link_rewrite, $imageIds, 'cart_default')|escape:'html':'UTF-8'}" alt="{$imageTitle}" title="{$imageTitle}"{if isset($cartSize)} height="{$cartSize.height}" width="{$cartSize.width}"{/if} itemprop="image" />
									</a>
								</li>
							{/foreach}
						{/if}
						</ul>
					</div> <!-- end thumbs_list -->
					{if isset($images) && count($images) > 2}
						<a id="view_scroll_right" title="{l s='Other views'}" href="javascript:{ldelim}{rdelim}">
							{l s='Next'}
						</a>
					{/if}
				</div> <!-- end views-block -->
				<!-- end thumbnails -->
			{/if}
			{if isset($images) && count($images) > 1}
				<p class="resetimg clear no-print" style="display:none;">
					<span id="wrapResetImages" style="display: none;">
						<a href="{$link->getProductLink($product)|escape:'html':'UTF-8'}" data-id="resetImages">
							<i class="icon-repeat"></i>
							{l s='Display all pictures'}
						</a>
					</span>
				</p>
			{/if}
		</div> <!-- end pb-left-column -->
		<!-- end left infos-->
		<!-- center infos -->
		<div class="pb-center-column col-xs-12 col-sm-4">
			{if $product->online_only}
				<p class="online_only">{l s='Online only'}</p>
			{/if}
			<h1 itemprop="name">{$product->name|escape:'html':'UTF-8'}</h1>
			{* <p id="product_reference"{if empty($product->reference) || !$product->reference} style="display: none;"{/if}>
				<label>{l s='Reference:'} </label>
				<span class="editable" itemprop="sku"{if !empty($product->reference) && $product->reference} content="{$product->reference}{/if}">{if !isset($groups)}{$product->reference|escape:'html':'UTF-8'}{/if}</span>
			</p> *}
			{* {if !$product->is_virtual && $product->condition}
			<p id="product_condition">
				<label>{l s='Condition:'} </label>
				{if $product->condition == 'new'}
					<link itemprop="itemCondition" href="http://schema.org/NewCondition"/>
					<span class="editable">{l s='New product'}</span>
				{elseif $product->condition == 'used'}
					<link itemprop="itemCondition" href="http://schema.org/UsedCondition"/>
					<span class="editable">{l s='Used'}</span>
				{elseif $product->condition == 'refurbished'}
					<link itemprop="itemCondition" href="http://schema.org/RefurbishedCondition"/>
					<span class="editable">{l s='Refurbished'}</span>
				{/if}
			</p>
			{/if} *}
			<div class="content_prices clearfix">
				{if $product->show_price && !isset($restricted_country_mode) && !$PS_CATALOG_MODE}
					<!-- prices -->
					<div>
						<p class="our_price_display" itemprop="offers" itemscope itemtype="http://schema.org/Offer">{strip}
							{if $product->quantity > 0}<link itemprop="availability" href="http://schema.org/InStock"/>{/if}
							{if $priceDisplay >= 0 && $priceDisplay <= 2}
								<span id="our_price_display" class="price" itemprop="price" content="{$productPrice}">{convertPrice price=$productPrice|floatval}</span>
								{if $tax_enabled  && ((isset($display_tax_label) && $display_tax_label == 1) || !isset($display_tax_label))}
									{if $priceDisplay == 1} {*l s='tax excl.'*}{else} {*l s='tax incl.'*}{/if}
								{/if}
								<meta itemprop="priceCurrency" content="{$currency->iso_code}" />
								{hook h="displayProductPriceBlock" product=$product type="price"}
							{/if}
						{/strip}
						</p>
						<p id="reduction_percent" {if !$product->specificPrice || $product->specificPrice.reduction_type != 'percentage'} style="display:none;"{/if}>{strip}
							<span id="reduction_percent_display">
								{if $product->specificPrice && $product->specificPrice.reduction_type == 'percentage'}-{$product->specificPrice.reduction*100}%{/if}
							</span>
						{/strip}</p>
						<p id="reduction_amount" {if !$product->specificPrice || $product->specificPrice.reduction_type != 'amount' || $product->specificPrice.reduction|floatval ==0} style="display:none"{/if}>{strip}
							<span id="reduction_amount_display">
							{if $product->specificPrice && $product->specificPrice.reduction_type == 'amount' && $product->specificPrice.reduction|floatval !=0}
								-{convertPrice price=$productPriceWithoutReduction|floatval-$productPrice|floatval}
							{/if}
							</span>
						{/strip}</p>
						<p id="old_price"{if (!$product->specificPrice || !$product->specificPrice.reduction)} class="hidden"{/if}>{strip}
							{if $priceDisplay >= 0 && $priceDisplay <= 2}
								{hook h="displayProductPriceBlock" product=$product type="old_price"}
								<span id="old_price_display"><span class="price">{if $productPriceWithoutReduction > $productPrice}{convertPrice price=$productPriceWithoutReduction|floatval}{/if}</span>{if $tax_enabled && $display_tax_label == 1} {if $priceDisplay == 1}{l s='tax excl.'}{else}{l s='tax incl.'}{/if}{/if}</span>
							{/if}
						{/strip}</p>
						{if $priceDisplay == 2}
							<br />
							<span id="pretaxe_price">{strip}
								<span id="pretaxe_price_display">{convertPrice price=$product->getPrice(false, $smarty.const.NULL)}</span> {l s='tax excl.'}
							{/strip}</span>
						{/if}
					</div> <!-- end prices -->
					{if $packItems|@count && $productPrice < $product->getNoPackPrice()}
						<p class="pack_price">{l s='Instead of'} <span style="text-decoration: line-through;">{convertPrice price=$product->getNoPackPrice()}</span></p>
					{/if}
					{if $product->ecotax != 0}
						<p class="price-ecotax">{l s='Including'} <span id="ecotax_price_display">{if $priceDisplay == 2}{$ecotax_tax_exc|convertAndFormatPrice}{else}{$ecotax_tax_inc|convertAndFormatPrice}{/if}</span> {l s='for ecotax'}
							{if $product->specificPrice && $product->specificPrice.reduction}
							<br />{l s='(not impacted by the discount)'}
							{/if}
						</p>
					{/if}
					{if !empty($product->unity) && $product->unit_price_ratio > 0.000000}
						{math equation="pprice / punit_price" pprice=$productPrice  punit_price=$product->unit_price_ratio assign=unit_price}
						<p class="unit-price"><span id="unit_price_display">{convertPrice price=$unit_price}</span> {l s='per'} {$product->unity|escape:'html':'UTF-8'}</p>
						{hook h="displayProductPriceBlock" product=$product type="unit_price"}
					{/if}
				{/if} {*close if for show price*}
				{hook h="displayProductPriceBlock" product=$product type="weight" hook_origin='product_sheet'}
										{hook h="displayProductPriceBlock" product=$product type="after_price"}
				<div class="clear"></div>
			</div> <!-- end content_prices -->
			{if $product->description_short || $packItems|@count > 0}
				<div id="short_description_block">
					{if $product->description_short}
						<div id="short_description_content" class="rte align_justify" itemprop="description">{$product->description_short}</div>
					{/if}

					{if $product->description}
						<p class="buttons_bottom_block">
							<a href="javascript:{ldelim}{rdelim}" class="button">
								{l s='More details'}
							</a>
						</p>
					{/if}
					<!--{if $packItems|@count > 0}
						<div class="short_description_pack">
						<h3>{l s='Pack content'}</h3>
							{foreach from=$packItems item=packItem}

							<div class="pack_content">
								{$packItem.pack_quantity} x <a href="{$link->getProductLink($packItem.id_product, $packItem.link_rewrite, $packItem.category)|escape:'html':'UTF-8'}">{$packItem.name|escape:'html':'UTF-8'}</a>
								<p>{$packItem.description_short}</p>
							</div>
							{/foreach}
						</div>
					{/if}-->
				</div> <!-- end short_description_block -->
			{/if}
			<div class="product_attributes clearfix">
				<!-- quantity wanted -->
				{if !$PS_CATALOG_MODE}
				<p id="quantity_wanted_p"{if (!$allow_oosp && $product->quantity <= 0) || !$product->available_for_order || $PS_CATALOG_MODE} style="display: none;"{/if}>
					<label for="quantity_wanted">{l s='Quantity'}</label>
					<input type="number" min="1" name="qty" id="quantity_wanted" class="text" value="{if isset($quantityBackup)}{$quantityBackup|intval}{else}{if $product->minimal_quantity > 1}{$product->minimal_quantity}{else}1{/if}{/if}" />
					<a href="#" data-field-qty="qty" class="btn btn-default button-minus product_quantity_down">
						<span><i class="icon-minus"></i></span>
					</a>
					<a href="#" data-field-qty="qty" class="btn btn-default button-plus product_quantity_up">
						<span><i class="icon-plus"></i></span>
					</a>
					<span class="clearfix"></span>
				</p>
				{/if}
				<!-- minimal quantity wanted -->
				<p id="minimal_quantity_wanted_p"{if $product->minimal_quantity <= 1 || !$product->available_for_order || $PS_CATALOG_MODE} style="display: none;"{/if}>
					{l s='The minimum purchase order quantity for the product is'} <b id="minimal_quantity_label">{$product->minimal_quantity}</b>
				</p>
				{if isset($groups)}
					<!-- attributes -->
					<div id="attributes" style="margin-bottom: 40px;">
						<div class="clearfix"></div>
						{$i = 0}
						{foreach from=$groups key=id_attribute_group item=group}
							{if $group.attributes|@count}
								<fieldset class="attribute_fieldset">
									<label class="attribute_label" {if $group.group_type != 'color' && $group.group_type != 'radio'}for="group_{$id_attribute_group|intval}"{/if}>{$group.name|escape:'html':'UTF-8'}&nbsp;</label>
									{assign var="groupName" value="group_$id_attribute_group"}
									<div class="attribute_list">
										{if ($group.group_type == 'select')}
											<select name="{$groupName}" id="group_{$id_attribute_group|intval}" class="form-control attribute_select no-print">
												{foreach from=$group.attributes key=id_attribute item=group_attribute}
													<option value="{$id_attribute|intval}"{if (isset($smarty.get.$groupName) && $smarty.get.$groupName|intval == $id_attribute) || $group.default == $id_attribute} selected="selected"{/if} title="{$group_attribute|escape:'html':'UTF-8'}">{$group_attribute|escape:'html':'UTF-8'}</option>
												{/foreach}
											</select>
										{elseif ($group.group_type == 'color')}
											<ul id="color_to_pick_list" class="clearfix">
												{assign var="default_colorpicker" value=""}
												{foreach from=$group.attributes key=id_attribute item=group_attribute}
													{assign var='img_color_exists' value=file_exists($col_img_dir|cat:$id_attribute|cat:'.jpg')}
													<li{if $group.default == $id_attribute} class="selected"{/if}>
														<a href="{$link->getProductLink($product)|escape:'html':'UTF-8'}" id="color_{$id_attribute|intval}" name="{$colors.$id_attribute.name|escape:'html':'UTF-8'}" class="color_pick{if ($group.default == $id_attribute)} selected{/if}"{if !$img_color_exists && isset($colors.$id_attribute.value) && $colors.$id_attribute.value} style="background:{$colors.$id_attribute.value|escape:'html':'UTF-8'};"{/if} title="{$colors.$id_attribute.name|escape:'html':'UTF-8'}">
															{if $img_color_exists}
																<img src="{$img_col_dir}{$id_attribute|intval}.jpg" alt="{$colors.$id_attribute.name|escape:'html':'UTF-8'}" title="{$colors.$id_attribute.name|escape:'html':'UTF-8'}" width="20" height="20" />
															{/if}
														</a>
													</li>
													{if ($group.default == $id_attribute)}
														{$default_colorpicker = $id_attribute}
													{/if}
												{/foreach}
											</ul>
											<input type="hidden" class="color_pick_hidden" name="{$groupName|escape:'html':'UTF-8'}" value="{$default_colorpicker|intval}" />
										{elseif ($group.group_type == 'radio')}
											<ul>
												{foreach from=$group.attributes key=id_attribute item=group_attribute}
													<li>
														<input type="radio" class="attribute_radio" name="{$groupName|escape:'html':'UTF-8'}" value="{$id_attribute}" {if ($group.default == $id_attribute)} checked="checked"{/if} />
														<span>{$group_attribute|escape:'html':'UTF-8'}</span>
													</li>
												{/foreach}
											</ul>
										{/if}
									</div> <!-- end attribute_list -->
								</fieldset>

								{if $i++==0}								<p class="atributo1">{l s='Texto promoción complementos'}</p>{/if}
 {/if}{*$id_attribute} {$group_attribute} {$groupName*}
								{if ($groupName=='group_4' OR $groupName=='group_14')}

								<div id="enlaces-guias">
								<a id="calcular-mi-talla" href="#contenido-cmt" class="izquierda subrayado">{l s='Calcular mi talla'}</a>

								<div style="width:auto;height:auto;overflow: auto;position:relative; display: none;"><div id="contenido-nett" style="width:600px !important;">
				{include file="$tpl_dir./contact-form2.tpl"}


</div></div>

<div style="width:auto;height:auto;overflow: auto;position:relative; display: none;"><div id="contenido-gdt" style="width:750px !important;">
	<table>

		<tr>
			<td>Talla ESP<br />&nbsp;</td>
			<td>Bajo pecho<br />&nbsp;</td>
			<td>Sobre pecho<br />Copa A</td>
			<td><br />Copa B</td>
			<td><br />Copa C</td>
			<td><br />Copa D</td>
			<td><br />Copa E</td>
			<td><br />Copa F</td>
		</tr>

		<tr>
			<td>80</td>
			<td>64<br />65<br />66<br />67<br />68</td>
			<td>76,77,78<br />77,78,79<br />78,79,80<br />79,80,81<br />80,81,82</td>
			<td>79,80,81<br />80,81,82<br />81,82,83<br />82,83,84<br />83,84,85</td>
			<td>82,83,84<br />83,84,85<br />84,85,86<br />85,86,87<br />86,87,88</td>
			<td>85,86,87<br />86,87,88<br />87,88,89<br />88,89,90<br />89,90,91</td>
			<td>88,89,90<br />89,90,91<br />90,91,92<br />91,92,93<br />92,93,94</td>
			<td>91,92,93<br />92,93,94<br />93,94,95<br />94,95,96<br />95,96,97</td>
		</tr>

		<tr>
			<td>85</td>
			<td>69<br />70<br />71<br />72<br />73</td>
			<td>81,82,83<br />82,83,84<br />83,84,85<br />84,85,86<br />85,86,87</td>
			<td>84,85,86<br />85,86,87<br />86,87,88<br />87,88,89<br />88,89,90</td>
			<td>87,88,89<br />88,89,90<br />89,90,91<br />90,91,92<br />91,92,93</td>
			<td>90,91,92<br />91,92,93<br />92,93,94<br />93,94,95<br />94,95,96</td>
			<td>93,94,95<br />94,95,96<br />95,96,97<br />96,97,98<br />97,98,99</td>
			<td>96,97,98<br />97,98,99<br />98,99,100<br />99,100,101<br />100,101,102</td>
		</tr>

		<tr>
			<td>90</td>
			<td>74<br />75<br />76<br />77<br />78</td>
			<td>86,87,88<br />87,88,89<br />88,89,90<br />89,90,91<br />90,91,92</td>
			<td>89,90,91<br />90,91,92<br />91,92,93<br />92,93,94<br />93,94,95</td>
			<td>92,93,94<br />93,94,95<br />94,95,96<br />95,96,97<br />96,97,98</td>
			<td>95,96,97<br />96,97,98<br />97,98,99<br />98,99,100<br />99,100,101</td>
			<td>98,99,100<br />99,100,101<br />100,101,102<br />101,102,103<br />102,103,104</td>
			<td>101,102,103<br />102,103,104<br />103,104,105<br />104,105,106<br />105,106,107</td>
		</tr>

		<tr>
			<td>95</td>
			<td>79<br />80<br />81<br />82<br />83</td>
			<td>91,92,93<br />92,93,94<br />93,94,95<br />94,95,96<br />95,96,97</td>
			<td>94,95,96<br />95,96,97<br />96,97,98<br />97,98,99<br />98,99,100</td>
			<td>97,98,99<br />98,99,100<br />99,100,101<br />100,101,102<br />101,102,103</td>
			<td>100,101,102<br />101,102,103<br />102,103,104<br />103,104,105<br />104,105,106</td>
			<td>103,104,105<br />104,105,106<br />105,106,107<br />106,107,108<br />107,108,109</td>
			<td>106,107,108<br />107,108,109<br />108,109,110<br />109,110,111<br />110,111,112</td>
		</tr>

		<tr>
			<td>100</td>
			<td>84<br />85<br />86<br />87<br />88</td>
			<td>96,97,98<br />97,98,99<br />98,99,100<br />99,100,101<br />100,101,102</td>
			<td>99,100,101<br />100,101,102<br />101,102,103<br />102,103,104<br />103,104,105</td>
			<td>102,103,104<br />103,104,105<br />104,105,106<br />105,106,107<br />106,107,108</td>
			<td>105,106,107<br />106,107,108<br />107,108,109<br />108,109,110<br />109,110,111</td>
			<td>108,109,110<br />109,110,111<br />110,111,112<br />111,112,113<br />112,113,114</td>
			<td>111,112,113<br />112,113,114<br />113,114,115<br />114,115,116<br />115,116,117</td>
		</tr>

		<tr>
			<td>105</td>
			<td>89<br />90<br />91<br />92<br />93</td>
			<td>101,102,103<br />102,103,104<br />103,104,105<br />104,105,106<br />105,106,107</td>
			<td>104,105,106<br />105,106,107<br />106,107,108<br />107,108,109<br />108,109,110</td>
			<td>107,108,109<br />108,109,110<br />109,110,111<br />110,111,112<br />111,112,113</td>
			<td>110,111,112<br />111,112,113<br />112,113,114<br />113,114,115<br />114,115,116</td>
			<td>113,114,115<br />114,115,116<br />115,116,117<br />116,117,118<br />117,118,119</td>
			<td>116,117,118<br />117,118,119<br />118,119,120<br />119,120,121<br />120,121,122</td>
		</tr>

	</table>

</div></div>

<div style="width:auto;height:auto;overflow: auto;position:relative; display: none;"><div id="contenido-cmt" style="width:700px !important;">

	<h2 id="4" class="titulo">Encuentra tu talla de sujetador</h2>

        <p style="margin-top:20px;">Elige tu talla de contorno:
<select name="contorno" id="contorno">
    <option value="0">---</option>
    <option value="64">64</option>
    <option value="65">65</option>
    <option value="66">66</option>
    <option value="67">67</option>
    <option value="68">68</option>
    <option value="69">69</option>
    <option value="70">70</option>
    <option value="71">71</option>
    <option value="72">72</option>
    <option value="73">73</option>
    <option value="74">74</option>
    <option value="75">75</option>
    <option value="76">76</option>
    <option value="77">77</option>
    <option value="78">78</option>
    <option value="79">79</option>
    <option value="80">80</option>
    <option value="81">81</option>
    <option value="82">82</option>
    <option value="83">83</option>
    <option value="84">84</option>
    <option value="85">85</option>
    <option value="86">86</option>
    <option value="87">87</option>
    <option value="88">88</option>
    <option value="89">89</option>
    <option value="90">90</option>
    <option value="91">91</option>
    <option value="92">92</option>
    <option value="93">93</option>
</select></p>
<p>Elige tu talla de pecho:
<select name="pecho" id="pecho">
    <option value="0">---</option>
    <option value="1">76</option>
    <option value="2">77</option>
    <option value="3">78</option>
     <option value="4">79</option>
      <option value="5">80</option>
       <option value="6">81</option>
        <option value="7">82</option>
 <option value="8">83</option>
 <option value="9">84</option>
 <option value="10">85</option>
 <option value="11">86</option>
 <option value="12">87</option>
 <option value="13">88</option>
 <option value="14">89</option>
  <option value="15">90</option>
  <option value="16">91</option>
  <option value="17">92</option>
  <option value="18">93</option>
</select></p>

<p>Talla y copa:
<textarea name="resultado-talla" id="resultado-talla"></textarea>
<textarea name="resultado-copa" id="resultado-copa"></textarea>
</p>


</div></div>

								<a id="guia-de-tallas" href="#contenido-gdt" class="derecha subrayado">{l s='Guía de tallas'}</a>
								<div class="clearfix"></div>
								<a id="no-encuentras-tu-talla" href="#contenido-nett" class="izquierda subrayado">{l s='¿No encuentras tu talla?'}</a></div>

							{*/if*}
							{/if}

						{/foreach}
					</div> <!-- end attributes -->
				{/if}
			</div> <!-- end product_attributes -->
			<form id="buy_block"{if $PS_CATALOG_MODE && !isset($groups) && $product->quantity > 0} class="hidden"{/if} action="{$link->getPageLink('cart')|escape:'html':'UTF-8'}" method="post">
				<!-- hidden datas -->
				<p class="hidden">
					<input type="hidden" name="token" value="{$static_token}" />
					<input type="hidden" name="id_product" value="{$product->id|intval}" id="product_page_product_id" />
					<input type="hidden" name="add" value="1" />
					<input type="hidden" name="id_product_attribute" id="idCombination" value="" />
				</p>
				<!-- <div class="box-info-product"> -->



						<div id="boton-carro"{if (!$allow_oosp && $product->quantity <= 0) || !$product->available_for_order || (isset($restricted_country_mode) && $restricted_country_mode) || $PS_CATALOG_MODE} class="unvisible"{/if}>
							<p id="add_to_cart" class="buttons_bottom_block no-print">
								<button type="submit" name="Submit" class="exclusive">
									<span>{if $content_only && (isset($product->customization_required) && $product->customization_required)}{l s='Customize'}{else}{l s='Add to cart'}{/if}</span>
								</button>
							</p>
						</div>

					 <!-- end box-cart-bottom -->
				 <!-- end box-info-product -->
			</form>

			{* AQUI VA EL WHISLIST *}
			{if isset($HOOK_PRODUCT_ACTIONS) && $HOOK_PRODUCT_ACTIONS}{$HOOK_PRODUCT_ACTIONS}{/if}
			<div>

		  <!-- Nav tabs -->
		  <ul class="nav nav-tabs" role="tablist">
		    <li role="presentation" class="active"><a href="#ficha" aria-controls="ficha" role="tab" data-toggle="tab">Materiales</a></li>
		    <li role="presentation"><a href="#mas" aria-controls="mas" role="tab" data-toggle="tab">Nota de diseño</a></li>
		    <!--<li role="presentation"><a href="#resena" aria-controls="resena" role="tab" data-toggle="tab">Reseña</a></li>-->
		  </ul>

		  <!-- Tab panes -->
		  <div class="tab-content">
		    <div role="tabpanel" class="tab-pane fade in active" id="ficha">
				{if isset($features) && $features}
					<!-- Data sheet -->
					<section class="page-product-box">
						{* <h3 class="page-product-heading">{l s='Data sheet'}</h3> *}
						<table class="table-data-sheet_no">
							{foreach from=$features item=feature}
							<tr class="{cycle values="odd,even"}">
								{if isset($feature.value)}
								<!--<td>{$feature.name|escape:'html':'UTF-8'}</td>-->
								<td>{$feature.value|escape:'html':'UTF-8'}</td>
								{/if}
							</tr>
							{/foreach}
						</table>
					</section>
					<!--end Data sheet -->
				{/if}

		    </div>
		    <div role="tabpanel" class="tab-pane fade" id="mas">

							{if isset($product) && $product->description}
								<!-- More info -->
								<section class="page-product-box">
									{* <h3 class="page-product-heading">{l s='More info'}</h3> *}
									<!-- full description -->
									<div  class="rte">{$product->description}</div>
								</section>
								<!--end  More info -->
							{/if}
				</div>
		    <div role="tabpanel" class="tab-pane fade" id="resena">
					<!--HOOK_PRODUCT_TAB -->
					<section class="page-product-box">
						{$HOOK_PRODUCT_TAB}
						{if isset($HOOK_PRODUCT_TAB_CONTENT) && $HOOK_PRODUCT_TAB_CONTENT}{$HOOK_PRODUCT_TAB_CONTENT}{/if}
					</section>
					<!--end HOOK_PRODUCT_TAB -->
		    </div>
		  </div>


		</div>

{if isset($belvg_textarea)}
<div class="videoWrapper">
{$belvg_textarea}
</div>
{/if}

			<div id="loSocial">
					<ul  class="socialsharing_product list-inline no-print">

							<li data-type="twitter" data-nombre="{$product->name}" data-url="{$link->getProductLink($product)}" data-image="{$link->getImageLink($product->link_rewrite, $cover.id_image, 'large_default')|escape:'html':'UTF-8'}" type="button" class="twitter social-sharing">
								<i class="icon-twitter"><span>prueba</span></i>
								<!-- <img src="{$link->getMediaLink("`$module_dir`img/twitter.gif")}" alt="Tweet" /> -->
							</li>


							<li data-type="facebook" data-nombre="{$product->name}" data-url="{$link->getProductLink($product)}" data-image="{$link->getImageLink($product->link_rewrite, $cover.id_image, 'large_default')|escape:'html':'UTF-8'}" type="button" class="facebook social-sharing">
								<i class="icon-facebook"><span>prueba</span></i>
								<!-- <img src="{$link->getMediaLink("`$module_dir`img/facebook.gif")}" alt="Facebook Like" /> -->
							</li>

							<li data-type="google-plus" data-nombre="{$product->name}" data-url="{$link->getProductLink($product)}" data-image="{$link->getImageLink($product->link_rewrite, $cover.id_image, 'large_default')|escape:'html':'UTF-8'}" type="button" class="google-plus social-sharing">
								<i class="icon-google-plus"><span>prueba</span></i>
								<!-- <img src="{$link->getMediaLink("`$module_dir`img/google.gif")}" alt="Google Plus" /> -->
							</li>

							<li data-type="pinterest" data-nombre="{$product->name}" data-url="{$link->getProductLink($product)}" data-image="{$link->getImageLink($product->link_rewrite, $cover.id_image, 'large_default')|escape:'html':'UTF-8'}" type="button" class="pinterest social-sharing">
								<i class="icon-pinterest"><span>prueba</span></i>
								<!-- <img src="{$link->getMediaLink("`$module_dir`img/pinterest.gif")}" alt="Pinterest" /> -->
							</li>
							<div class="clearfix"></div>
					</ul>
			</div>
			{* {if ($display_qties == 1 && !$PS_CATALOG_MODE && $PS_STOCK_MANAGEMENT && $product->available_for_order)}
				<!-- number of item in stock -->
				<p id="pQuantityAvailable"{if $product->quantity <= 0} style="display: none;"{/if}>
					<span id="quantityAvailable">{$product->quantity|intval}</span>
					<span {if $product->quantity > 1} style="display: none;"{/if} id="quantityAvailableTxt">{l s='Item'}</span>
					<span {if $product->quantity == 1} style="display: none;"{/if} id="quantityAvailableTxtMultiple">{l s='Items'}</span>
				</p>
			{/if} *}
			<!-- availability or doesntExist -->
			{* <p id="availability_statut"{if !$PS_STOCK_MANAGEMENT || ($product->quantity <= 0 && !$product->available_later && $allow_oosp) || ($product->quantity > 0 && !$product->available_now) || !$product->available_for_order || $PS_CATALOG_MODE} style="display: none;"{/if}> *}
				{*<span id="availability_label">{l s='Availability:'}</span>*}
				{* <span id="availability_value" class="label{if $product->quantity <= 0 && !$allow_oosp} label-danger{elseif $product->quantity <= 0} label-warning{else} label-success{/if}">{if $product->quantity <= 0}{if $PS_STOCK_MANAGEMENT && $allow_oosp}{$product->available_later}{else}{l s='This product is no longer in stock'}{/if}{elseif $PS_STOCK_MANAGEMENT}{$product->available_now}{/if}</span> *}
			{* </p> *}
			{if $PS_STOCK_MANAGEMENT}
				{if !$product->is_virtual}{hook h="displayProductDeliveryTime" product=$product}{/if}
				<p class="warning_inline" id="last_quantities"{if ($product->quantity > $last_qties || $product->quantity <= 0) || $allow_oosp || !$product->available_for_order || $PS_CATALOG_MODE} style="display: none"{/if} >{l s='Warning: Last items in stock!'}</p>
			{/if}
			<p id="availability_date"{if ($product->quantity > 0) || !$product->available_for_order || $PS_CATALOG_MODE || !isset($product->available_date) || $product->available_date < $smarty.now|date_format:'%Y-%m-%d'} style="display: none;"{/if}>
				<span id="availability_date_label">{l s='Availability date:'}</span>
				<span id="availability_date_value">{if Validate::isDate($product->available_date)}{dateFormat date=$product->available_date full=false}{/if}</span>
			</p>
			<!-- Out of stock hook -->
			<div id="oosHook"{if $product->quantity > 0} style="display: none;"{/if}>
				{$HOOK_PRODUCT_OOS}
			</div>
			{if isset($HOOK_EXTRA_RIGHT) && $HOOK_EXTRA_RIGHT}{$HOOK_EXTRA_RIGHT}{/if}
			{if !$content_only}
				<!-- usefull links-->
				{* <ul id="usefull_link_block" class="clearfix no-print">
					{if $HOOK_EXTRA_LEFT}{$HOOK_EXTRA_LEFT}{/if}
					<li class="print">
						<a href="javascript:print();">
							{l s='Print'}
						</a>
					</li>
				</ul> *}
			{/if}
		</div>
		<!-- end center infos-->
		<!-- pb-right-column-->
		{* <div class="pb-right-column col-xs-12 col-sm-4 col-md-3">
			{if ($product->show_price && !isset($restricted_country_mode)) || isset($groups) || $product->reference || (isset($HOOK_PRODUCT_ACTIONS) && $HOOK_PRODUCT_ACTIONS)}
			<!-- add to cart form-->
			<form id="buy_block"{if $PS_CATALOG_MODE && !isset($groups) && $product->quantity > 0} class="hidden"{/if} action="{$link->getPageLink('cart')|escape:'html':'UTF-8'}" method="post">
				<!-- hidden datas -->
				<p class="hidden">
					<input type="hidden" name="token" value="{$static_token}" />
					<input type="hidden" name="id_product" value="{$product->id|intval}" id="product_page_product_id" />
					<input type="hidden" name="add" value="1" />
					<input type="hidden" name="id_product_attribute" id="idCombination" value="" />
				</p>
				<div class="box-info-product">


					<div class="box-cart-bottom">


					</div> <!-- end box-cart-bottom -->
				</div> <!-- end box-info-product -->
			</form>
			{/if}
		</div> <!-- end pb-right-column--> *}
	</div> <!-- end primary_block -->
	{if !$content_only}
{if (isset($quantity_discounts) && count($quantity_discounts) > 0)}
			<!-- quantity discount -->
			<!--<section class="page-product-box">
				<h3 class="page-product-heading">{l s='Volume discounts'}</h3>
				<div id="quantityDiscount">
					<table class="std table-product-discounts">
						<thead>
							<tr>
								<th>{l s='Quantity'}</th>
								<th>{if $display_discount_price}{l s='Price'}{else}{l s='Discount'}{/if}</th>
								<th>{l s='You Save'}</th>
							</tr>
						</thead>
						<tbody>
							{foreach from=$quantity_discounts item='quantity_discount' name='quantity_discounts'}
							<tr id="quantityDiscount_{$quantity_discount.id_product_attribute}" class="quantityDiscount_{$quantity_discount.id_product_attribute}" data-discount-type="{$quantity_discount.reduction_type}" data-discount="{$quantity_discount.real_value|floatval}" data-discount-quantity="{$quantity_discount.quantity|intval}">
								<td>
									{$quantity_discount.quantity|intval}
								</td>
								<td>
									{if $quantity_discount.price >= 0 || $quantity_discount.reduction_type == 'amount'}
										{if $display_discount_price}
											{if $quantity_discount.reduction_tax == 0 && !$quantity_discount.price}
												{convertPrice price = $productPriceWithoutReduction|floatval-($productPriceWithoutReduction*$quantity_discount.reduction_with_tax)|floatval}
											{else}
												{convertPrice price=$productPriceWithoutReduction|floatval-$quantity_discount.real_value|floatval}
											{/if}
										{else}
											{convertPrice price=$quantity_discount.real_value|floatval}
										{/if}
									{else}
										{if $display_discount_price}
											{if $quantity_discount.reduction_tax == 0}
												{convertPrice price = $productPriceWithoutReduction|floatval-($productPriceWithoutReduction*$quantity_discount.reduction_with_tax)|floatval}
											{else}
												{convertPrice price = $productPriceWithoutReduction|floatval-($productPriceWithoutReduction*$quantity_discount.reduction)|floatval}
											{/if}
										{else}
											{$quantity_discount.real_value|floatval}%
										{/if}
									{/if}
								</td>
								<td>
									<span>{l s='Up to'}</span>
									{if $quantity_discount.price >= 0 || $quantity_discount.reduction_type == 'amount'}
										{$discountPrice=$productPriceWithoutReduction|floatval-$quantity_discount.real_value|floatval}
									{else}
										{$discountPrice=$productPriceWithoutReduction|floatval-($productPriceWithoutReduction*$quantity_discount.reduction)|floatval}
									{/if}
									{$discountPrice=$discountPrice * $quantity_discount.quantity}
									{$qtyProductPrice=$productPriceWithoutReduction|floatval * $quantity_discount.quantity}
									{convertPrice price=$qtyProductPrice - $discountPrice}
								</td>
							</tr>
							{/foreach}
						</tbody>
					</table>
				</div>
			</section>
		{/if}-->


		{if isset($packItems) && $packItems|@count > 0}
		<section id="blockpack">
			<h3 class="page-product-heading">{l s='Pack content'}</h3>
			{include file="$tpl_dir./product-list.tpl" products=$packItems}
		</section>
		{/if}

		{if isset($accessories) && $accessories}

		<h4 class="peleproductsrelated_h3 page-product-heading h4 optima"><span>{l s='A JUEGO CON' mod='pele_relatedproducts'}</span></h4>

	<div id="texto-conjuntos">
<p>{l s='Texto a juego con' mod='pele_relatedproducts'}</p>
</div>

			<!--Accessories -->
			<section class="page-product-box">
				<!--<h3 class="page-product-heading">{l s='Accessories'}</h3>-->
				<div class="block products_block accessories-block clearfix" style="text-align: center !important;">
					<div class="block_content" style="text-align: center !important; float: none !important;">
						<ul id="bxslider" class="bxslider clearfix" style="margin:0 auto !important; display: inline-table !important; width:100% !important; float: none !important;">
							{foreach from=$accessories item=accessory name=accessories_list}
								{if ($accessory.allow_oosp || $accessory.quantity_all_versions > 0 || $accessory.quantity > 0) && $accessory.available_for_order && !isset($restricted_country_mode)}
									{assign var='accessoryLink' value=$link->getProductLink($accessory.id_product, $accessory.link_rewrite, $accessory.category)}
									<li class="item pproduct-box ajax_block_product{if $smarty.foreach.accessories_list.first} first_item{elseif $smarty.foreach.accessories_list.last} last_item{else} item{/if} product_accessories_description" style="display: inline-block !important; float: none !important;">
										<div class="product_desc">
											<a href="{$accessoryLink|escape:'html':'UTF-8'}" title="{$accessory.legend|escape:'html':'UTF-8'}" class="product-image product_image">
												<img class="lazyOwl" src="{$link->getImageLink($accessory.link_rewrite, $accessory.id_image, 'home_default')|escape:'html':'UTF-8'}" alt="{$accessory.legend|escape:'html':'UTF-8'}" width="360" height="auto" />
											</a>
											<div class="block_description">
												<a href="{$accessoryLink|escape:'html':'UTF-8'}" title="{l s='More'}" class="product_description">
													{$accessory.description_short|strip_tags|truncate:25:'...'}
												</a>
											</div>
										</div>
										<div class="s_title_block">
											<h5 itemprop="name" class="product-name">
												<a href="{$accessoryLink|escape:'html':'UTF-8'}">
													{$accessory.name|truncate:20:'...':true|escape:'html':'UTF-8'}
												</a>
											</h5>
											{if $accessory.show_price && !isset($restricted_country_mode) && !$PS_CATALOG_MODE}
											<span class="price">
												{if $priceDisplay != 1}
												{displayWtPrice p=$accessory.price}{else}{displayWtPrice p=$accessory.price_tax_exc}
												{/if}
											</span>
											{/if}
										</div>
										<div class="clearfix" style="margin-top:5px">
											<!--{if !$PS_CATALOG_MODE && ($accessory.allow_oosp || $accessory.quantity > 0)}
												<div class="no-print">
													<a class="exclusive button ajax_add_to_cart_button" href="{$link->getPageLink('cart', true, NULL, "qty=1&amp;id_product={$accessory.id_product|intval}&amp;token={$static_token}&amp;add")|escape:'html':'UTF-8'}" data-id-product="{$accessory.id_product|intval}" title="{l s='Add to cart'}">
														<span>{l s='Add to cart'}</span>
													</a>
												</div>
											{/if}-->
										</div>
									</li>
								{/if}
							{/foreach}
						</ul>
					</div>
				</div>
			</section>
			<!--end Accessories -->
		{/if}
		<!--HOOK_PRODUCT_TAB -->
		{* <section class="page-product-box">
			{$HOOK_PRODUCT_TAB}
			{if isset($HOOK_PRODUCT_TAB_CONTENT) && $HOOK_PRODUCT_TAB_CONTENT}{$HOOK_PRODUCT_TAB_CONTENT}{/if}
		</section> *}
		<!--end HOOK_PRODUCT_TAB -->
		{if isset($HOOK_PRODUCT_FOOTER) && $HOOK_PRODUCT_FOOTER}{$HOOK_PRODUCT_FOOTER}{/if}
		<!-- description & features -->
		{if (isset($product) && $product->description) || (isset($features) && $features) || (isset($accessories) && $accessories) || (isset($HOOK_PRODUCT_TAB) && $HOOK_PRODUCT_TAB) || (isset($attachments) && $attachments) || isset($product) && $product->customizable}
			{if isset($attachments) && $attachments}
			<!--Download -->
			<section class="page-product-box">
				<h3 class="page-product-heading">{l s='Download'}</h3>
				{foreach from=$attachments item=attachment name=attachements}
					{if $smarty.foreach.attachements.iteration %3 == 1}<div class="row">{/if}
						<div class="col-lg-4">
							<h4><a href="{$link->getPageLink('attachment', true, NULL, "id_attachment={$attachment.id_attachment}")|escape:'html':'UTF-8'}">{$attachment.name|escape:'html':'UTF-8'}</a></h4>
							<p class="text-muted">{$attachment.description|escape:'html':'UTF-8'}</p>
							<a class="btn btn-default btn-block" href="{$link->getPageLink('attachment', true, NULL, "id_attachment={$attachment.id_attachment}")|escape:'html':'UTF-8'}">
								<i class="icon-download"></i>
								{l s="Download"} ({Tools::formatBytes($attachment.file_size, 2)})
							</a>
							<hr />
						</div>
					{if $smarty.foreach.attachements.iteration %3 == 0 || $smarty.foreach.attachements.last}</div>{/if}
				{/foreach}
			</section>
			<!--end Download -->
			{/if}
			{if isset($product) && $product->customizable}
			<!--Customization -->
			<section class="page-product-box">
				<h3 class="page-product-heading">{l s='Product customization'}</h3>
				<!-- Customizable products -->
				<form method="post" action="{$customizationFormTarget}" enctype="multipart/form-data" id="customizationForm" class="clearfix">
					<p class="infoCustomizable">
						{l s='After saving your customized product, remember to add it to your cart.'}
						{if $product->uploadable_files}
						<br />
						{l s='Allowed file formats are: GIF, JPG, PNG'}{/if}
					</p>
					{if $product->uploadable_files|intval}
						<div class="customizableProductsFile">
							<h5 class="product-heading-h5">{l s='Pictures'}</h5>
							<ul id="uploadable_files" class="clearfix">
								{counter start=0 assign='customizationField'}
								{foreach from=$customizationFields item='field' name='customizationFields'}
									{if $field.type == 0}
										<li class="customizationUploadLine{if $field.required} required{/if}">{assign var='key' value='pictures_'|cat:$product->id|cat:'_'|cat:$field.id_customization_field}
											{if isset($pictures.$key)}
												<div class="customizationUploadBrowse">
													<img src="{$pic_dir}{$pictures.$key}_small" alt="" />
														<a href="{$link->getProductDeletePictureLink($product, $field.id_customization_field)|escape:'html':'UTF-8'}" title="{l s='Delete'}" >
															<img src="{$img_dir}icon/delete.gif" alt="{l s='Delete'}" class="customization_delete_icon" width="11" height="13" />
														</a>
												</div>
											{/if}
											<div class="customizationUploadBrowse form-group">
												<label class="customizationUploadBrowseDescription">
													{if !empty($field.name)}
														{$field.name}
													{else}
														{l s='Please select an image file from your computer'}
													{/if}
													{if $field.required}<sup>*</sup>{/if}
												</label>
												<input type="file" name="file{$field.id_customization_field}" id="img{$customizationField}" class="form-control customization_block_input {if isset($pictures.$key)}filled{/if}" />
											</div>
										</li>
										{counter}
									{/if}
								{/foreach}
							</ul>
						</div>
					{/if}
					{if $product->text_fields|intval}
						<div class="customizableProductsText">
							<h5 class="product-heading-h5">{l s='Text'}</h5>
							<ul id="text_fields">
							{counter start=0 assign='customizationField'}
							{foreach from=$customizationFields item='field' name='customizationFields'}
								{if $field.type == 1}
									<li class="customizationUploadLine{if $field.required} required{/if}">
										<label for ="textField{$customizationField}">
											{assign var='key' value='textFields_'|cat:$product->id|cat:'_'|cat:$field.id_customization_field}
											{if !empty($field.name)}
												{$field.name}
											{/if}
											{if $field.required}<sup>*</sup>{/if}
										</label>
										<textarea name="textField{$field.id_customization_field}" class="form-control customization_block_input" id="textField{$customizationField}" rows="3" cols="20">{strip}
											{if isset($textFields.$key)}
												{$textFields.$key|stripslashes}
											{/if}
										{/strip}</textarea>
									</li>
									{counter}
								{/if}
							{/foreach}
							</ul>
						</div>
					{/if}
					<p id="customizedDatas">
						<input type="hidden" name="quantityBackup" id="quantityBackup" value="" />
						<input type="hidden" name="submitCustomizedDatas" value="1" />
						<button class="button btn btn-default button button-small" name="saveCustomization">
							<span>{l s='Save'}</span>
						</button>
						<span id="ajax-loader" class="unvisible">
							<img src="{$img_ps_dir}loader.gif" alt="loader" />
						</span>
					</p>
				</form>
				<p class="clear required"><sup>*</sup> {l s='required fields'}</p>
			</section>
			<!--end Customization -->
			{/if}
		{/if}
	{/if}
</div> <!-- itemscope product wrapper -->
{strip}
{if isset($smarty.get.ad) && $smarty.get.ad}
	{addJsDefL name=ad}{$base_dir|cat:$smarty.get.ad|escape:'html':'UTF-8'}{/addJsDefL}
{/if}
{if isset($smarty.get.adtoken) && $smarty.get.adtoken}
	{addJsDefL name=adtoken}{$smarty.get.adtoken|escape:'html':'UTF-8'}{/addJsDefL}
{/if}
{addJsDef allowBuyWhenOutOfStock=$allow_oosp|boolval}
{addJsDef availableNowValue=$product->available_now|escape:'quotes':'UTF-8'}
{addJsDef availableLaterValue=$product->available_later|escape:'quotes':'UTF-8'}
{addJsDef attribute_anchor_separator=$attribute_anchor_separator|escape:'quotes':'UTF-8'}
{addJsDef attributesCombinations=$attributesCombinations}
{addJsDef currentDate=$smarty.now|date_format:'%Y-%m-%d %H:%M:%S'}
{if isset($combinations) && $combinations}
	{addJsDef combinations=$combinations}
	{addJsDef combinationsFromController=$combinations}
	{addJsDef displayDiscountPrice=$display_discount_price}
	{addJsDefL name='upToTxt'}{l s='Up to' js=1}{/addJsDefL}
{/if}
{if isset($combinationImages) && $combinationImages}
	{addJsDef combinationImages=$combinationImages}
{/if}
{addJsDef customizationId=$id_customization}
{addJsDef customizationFields=$customizationFields}
{addJsDef default_eco_tax=$product->ecotax|floatval}
{addJsDef displayPrice=$priceDisplay|intval}
{addJsDef ecotaxTax_rate=$ecotaxTax_rate|floatval}
{if isset($cover.id_image_only)}
	{addJsDef idDefaultImage=$cover.id_image_only|intval}
{else}
	{addJsDef idDefaultImage=0}
{/if}
{addJsDef img_ps_dir=$img_ps_dir}
{addJsDef img_prod_dir=$img_prod_dir}
{addJsDef id_product=$product->id|intval}
{addJsDef jqZoomEnabled=$jqZoomEnabled|boolval}
{addJsDef maxQuantityToAllowDisplayOfLastQuantityMessage=$last_qties|intval}
{addJsDef minimalQuantity=$product->minimal_quantity|intval}
{addJsDef noTaxForThisProduct=$no_tax|boolval}
{if isset($customer_group_without_tax)}
	{addJsDef customerGroupWithoutTax=$customer_group_without_tax|boolval}
{else}
	{addJsDef customerGroupWithoutTax=false}
{/if}
{if isset($group_reduction)}
	{addJsDef groupReduction=$group_reduction|floatval}
{else}
	{addJsDef groupReduction=false}
{/if}
{addJsDef oosHookJsCodeFunctions=Array()}
{addJsDef productHasAttributes=isset($groups)|boolval}
{addJsDef productPriceTaxExcluded=($product->getPriceWithoutReduct(true)|default:'null' - $product->ecotax)|floatval}
{addJsDef productPriceTaxIncluded=($product->getPriceWithoutReduct(false)|default:'null' - $product->ecotax)|floatval}
{addJsDef productBasePriceTaxExcluded=($product->getPrice(false, null, 6, null, false, false) - $product->ecotax)|floatval}
{addJsDef productBasePriceTaxExcl=($product->getPrice(false, null, 6, null, false, false)|floatval)}
{addJsDef productBasePriceTaxIncl=($product->getPrice(true, null, 6, null, false, false)|floatval)}
{addJsDef productReference=$product->reference|escape:'html':'UTF-8'}
{addJsDef productAvailableForOrder=$product->available_for_order|boolval}
{addJsDef productPriceWithoutReduction=$productPriceWithoutReduction|floatval}
{addJsDef productPrice=$productPrice|floatval}
{addJsDef productUnitPriceRatio=$product->unit_price_ratio|floatval}
{addJsDef productShowPrice=(!$PS_CATALOG_MODE && $product->show_price)|boolval}
{addJsDef PS_CATALOG_MODE=$PS_CATALOG_MODE}
{if $product->specificPrice && $product->specificPrice|@count}
	{addJsDef product_specific_price=$product->specificPrice}
{else}
	{addJsDef product_specific_price=array()}
{/if}
{if $display_qties == 1 && $product->quantity}
	{addJsDef quantityAvailable=$product->quantity}
{else}
	{addJsDef quantityAvailable=0}
{/if}
{addJsDef quantitiesDisplayAllowed=$display_qties|boolval}
{if $product->specificPrice && $product->specificPrice.reduction && $product->specificPrice.reduction_type == 'percentage'}
	{addJsDef reduction_percent=$product->specificPrice.reduction*100|floatval}
{else}
	{addJsDef reduction_percent=0}
{/if}
{if $product->specificPrice && $product->specificPrice.reduction && $product->specificPrice.reduction_type == 'amount'}
	{addJsDef reduction_price=$product->specificPrice.reduction|floatval}
{else}
	{addJsDef reduction_price=0}
{/if}
{if $product->specificPrice && $product->specificPrice.price}
	{addJsDef specific_price=$product->specificPrice.price|floatval}
{else}
	{addJsDef specific_price=0}
{/if}
{addJsDef specific_currency=($product->specificPrice && $product->specificPrice.id_currency)|boolval} {* TODO: remove if always false *}
{addJsDef stock_management=$PS_STOCK_MANAGEMENT|intval}
{addJsDef taxRate=$tax_rate|floatval}
{addJsDefL name=doesntExist}{l s='This combination does not exist for this product. Please select another combination.' js=1}{/addJsDefL}
{addJsDefL name=doesntExistNoMore}{l s='This product is no longer in stock' js=1}{/addJsDefL}
{addJsDefL name=doesntExistNoMoreBut}{l s='with those attributes but is available with others.' js=1}{/addJsDefL}
{addJsDefL name=fieldRequired}{l s='Please fill in all the required fields before saving your customization.' js=1}{/addJsDefL}
{addJsDefL name=uploading_in_progress}{l s='Uploading in progress, please be patient.' js=1}{/addJsDefL}
{addJsDefL name='product_fileDefaultHtml'}{l s='No file selected' js=1}{/addJsDefL}
{addJsDefL name='product_fileButtonHtml'}{l s='Choose File' js=1}{/addJsDefL}
{/strip}
{/if}
