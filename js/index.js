/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
var pos_fotos = 1;
$(document).ready(function(){
	$('#home-page-tabs li:first, #index .tab-content ul:first').addClass('active');
	// $('li.social-sharing').on('click', function(){
	// 	type = $(this).attr('data-type');
	// 	sharing_name = $(this).attr('data-nombre');
	// 	sharing_url = $(this).attr('data-url');
	// 	if (type.length)
	// 	{
	// 		switch(type)
	// 		{
	// 			case 'twitter':
	// 				window.open('https://twitter.com/intent/tweet?text=' + sharing_name + ' ' + encodeURIComponent(sharing_url), 'sharertwt', 'toolbar=0,status=0,width=640,height=445');
	// 				break;
	// 			case 'facebook':
	// 				window.open('http://www.facebook.com/sharer.php?u=' + sharing_url, 'sharer', 'toolbar=0,status=0,width=660,height=445');
	// 				break;
	// 			case 'google-plus':
	// 				window.open('https://plus.google.com/share?url=' + sharing_url, 'sharer', 'toolbar=0,status=0,width=660,height=445');
	// 				break;
	// 			case 'pinterest':
	// 				window.open('http://www.pinterest.com/pin/create/button/?media=' + sharing_img + '&url=' + sharing_url, 'sharerpinterest', 'toolbar=0,status=0,width=660,height=445');
	// 				break;
	// 		}
	// 	}
	// });

	$(".foto_derecha").on("click", function(e){
		e.preventDefault();
		id_prod = $(this).attr('data-product-id');
		cant_fotos = $("#thumbs_list_frame_"+id_prod+" > li").length;
		if(pos_fotos < cant_fotos){
			next = pos_fotos+1;
		}else{
			next = 1;
		}
		imagen = $("#thumbs_list_frame_"+id_prod+" li:nth-child("+next+") a");
		// console.log(imagen.attr("href"));
		var new_src = imagen.attr('href').replace('thickbox', 'large');
		console.log(new_src);
		// var new_title = imagen.attr('title');
		var new_href = imagen.attr('href');
		if ($('#img_'+id_prod).attr('src') != new_src)
		{

			$('#img_'+id_prod).attr({
				'src' : new_src
			}).load(function(){
				// if (typeof(jqZoomEnabled) != 'undefined' && jqZoomEnabled)
				// 	$(this).attr('rel', new_href);
			});
		}
		if(pos_fotos < cant_fotos){
			pos_fotos++;
		}else{
			pos_fotos=1;
		}
	});
	$(".foto_izq").on("click", function(e){
		e.preventDefault();
		id_prod = $(this).attr('data-product-id');
		cant_fotos = $("#thumbs_list_frame_"+id_prod+" > li").length;
		if(pos_fotos == 1){
			next = cant_fotos;
		}else{
			next = pos_fotos-1;
		}
		imagen = $("#thumbs_list_frame_"+id_prod+" li:nth-child("+next+") a");
		// console.log(imagen.attr("href"));
		var new_src = imagen.attr('href').replace('thickbox', 'large');
		console.log(new_src);
		// var new_title = imagen.attr('title');
		var new_href = imagen.attr('href');
		if ($('#img_'+id_prod).attr('src') != new_src)
		{

			$('#img_'+id_prod).attr({
				'src' : new_src
			}).load(function(){
				// if (typeof(jqZoomEnabled) != 'undefined' && jqZoomEnabled)
				// 	$(this).attr('rel', new_href);
			});
		}
		if(pos_fotos == 1){
			pos_fotos = cant_fotos;
		}else{
			pos_fotos--;
		}
	});
});
